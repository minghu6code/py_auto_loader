# -*- Coding:utf-8 -*-
"""

"""

__all__ = ['auto_load_var',
           'findall_attr',
           'flatten']

import re
import os
from importlib import import_module
from collections import Iterable


def auto_load_var(package_name, module_pattern, variable_pattern, base_path=None):
    """

    >>> auto_load_var('minghu6.etc', 'fi.*', '[f|F].*')
    [<function minghu6.etc.find.find>,
     <function minghu6.etc.find.findlist>,
     <module 'fnmatch' from '/usr/lib/python3.5/fnmatch.py'>,
     minghu6.etc.fileformat.FileTypePair,
     <function minghu6.etc.fileformat.fileformat>]
    """
    if base_path is None:
        base_path = package_name.replace('.', os.sep)

    return list(
        flatten(map(lambda module_name: _load_var_from_module('%s.%s' % (package_name, module_name), variable_pattern),
                    _findall_module_name(base_path, module_pattern))))


def flatten(items):
    """Yield items from any nested iterable; see REF."""
    for x in items:
        if isinstance(x, Iterable) and not isinstance(x, (str, bytes)):
            yield from flatten(x)
        else:
            yield x


def findall_attr(obj, pattern):
    return [getattr(obj, attr_name) for attr_name in dir(obj)
            if re.match(pattern, attr_name)]


def _findall_module_name(base_path, pattern):
    return [os.path.splitext(fn)[0] for fn in os.listdir(base_path)
            if re.match(pattern, os.path.splitext(fn)[0])]


def _load_var_from_module(module_name, attrname_pattern):
    """get all attrtibute according to name pattern
    from a module(import from module name)
    """
    try:
        module = import_module(module_name)
    except ImportError:
        return []
    else:
        return findall_attr(module, attrname_pattern)